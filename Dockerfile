FROM node:6.11.3
RUN /bin/bash -c "apt-get update \
	&& apt-get install -y python-pip python-dev build-essential \
 	&& yes | pip install  --upgrade pip \
	&& yes | pip install awscli --upgrade --user \
	&& source ~/.bashrc"
